Non deterministic python
========================

A module to allow the usage of non-determinism within a Python
function.

It is not meant for efficient programming but to convey the idea of
non-determinism to students learning about NP.

It allows mostly to programs with it. It is based on backtracking
on the exploration on the space of guesses made during the execution
of the programs.


Non deterministic programs
==========================

A program is non-deterministic if along the way of its
execution it has some *magic power* to guess some information
to perform its own computation.

Lets take an example. Say we have a function with four argument
`f(x: bool, y: bool, z: bool, t:bool) -> bool`.

So we don't know what this function is doing but we want to find some values
such that it evaluates to `True`. Here is a non-deterministic programs for
that:


```python
from nondet import nondet
@nondet
def sat(guess, f) -> tuple[bool, bool, bool, bool]:
    guesses = (guess(), guess(), guess(), guess())
    if f(*guesses):
        return guesses 
    return None
```

This programs feed non-deterministic guess to `f` and simply return the value.
Because this programs uses non-determinism, the return value is not obvious.
If the guesses are correct we get what we want (some value for x,y,z and t that
make the function happy) but otherwise, we get `None`.

Here `None` means that the execution is not successful. So the correct Non-deterministic
returns of `sat` is either a valid answer if some exists and `None` otherwise.



Magical computer that guess for you some data of course doesn't exists. It is nevertheless
possible to simulate a non-deterministic algorithms by executing the function
for all possible choices of `guess` alongs all possible runs.

The deterministic variant of SAT above can be rewrited as follows:

```python
from itertools import product
def sat_det(f) -> tuple[bool, bool, bool, bool]:
    U = (True, False)
    for r in product(U, U, U, U):
        if f(*r):
            return r
    return None
``` 

This algorithms is not much more complicated and does exactly the
same as before. It does require writing an explicit exploration
of the domain of `f` while this exploration is handle automatically
by the non-determinism in the first example.

We can choose a slightly more complicated example. Let says
now that `f` admits a variadic amount of arguments.
The non-determinist version will be something like:

```python
@nondet
def sat_var(guess, f) -> tuple[bool, ...]:
    guesses = []
    while not f(*guesses):
        guesses.append(guess())
    return tuple(guesses)
```

The deterministic version is more complicated.
We need to enumerate all possible boolean values which
requires some work.


```python
def sat_var_det(f) -> tuple[bool, ...]:
    U = (True, False)
    L = []
    while True:
        for v in itertools.product(*L):
            if f(*v):
                return v
        L.append(U)
```

As we can seen the non-deterministic variant is simply nicer to write,
less operational and closer to the actual intent of the function.

Under the hoods
===============

The `nondet` module mostly provides a decorator to write
non-deterministic function. It will explore with a naive backtracking
algorithm all the possible values for the `guesses` performed
during the executions.

The code is simple:

A class `Guesser` store the current branch of the boolean tree
of all possible decision we are exploring. At the end of the execution,
if no valid answer if found, it will simply choose the next branch.



More interesting Examples
=========================

We can use the module for graph problems. Trying to find for instance
a k-clique in a graphe.

```python
from nondet import nondet
import networkx as nx

@nondet
def kclique(guess, G: nx.Graph, k: int) -> list | None:
    """
    Return a kclique if the input graph contains a k-clique and False otherwise 
    See: https://en.wikipedia.org/wiki/Clique_problem

    """
    possible_subset = list()
    for node in G:
        if guess(): # this returns a boolean
            for node2 in possible_subset:
                if not (node, node2) in G.edges:
                    return False 
            possible_subset.append(node)

        if len(possible_subset) == k:
            return possible_subset

    return None
``` 

Then, we can execute this function exactly as a deterministic one.

```python
K = nx.karate_club_graph()
kclique(K, 5) # return a click
kclique(K, 6) # return None
```

We can also perform an exhaustif search and iterate over all the results by
simply changing the decorator:

```python
@nondet_iter
def kclique(guess, G: nx.Graph, k: int) -> list | None:
    """
    Return a kclique if the input graph contains a k-clique and False otherwise 
    See: https://en.wikipedia.org/wiki/Clique_problem

    """
    possible_subset = list()
    for node in G:
        if guess(): # this returns a boolean
            for node2 in possible_subset:
                if not (node, node2) in G.edges:
                    return False 
            possible_subset.append(node)

        if len(possible_subset) == k:
            return possible_subset

    return None
```

Then now we iterate over all the founded clique:

```python
list(kclique_iter(K, 5)) # return the two 5-cliques of K
```

There is funny performance optimisation: you can of course first guess
a solution and then check it is correct (it is called guessing a certificate,
see [here](https://en.wikipedia.org/wiki/Certificate_(complexity)).

I have included into guess some utilities for that:

```python
@nondet
def kclique(guess, G: nx.Graph, k: int) -> list | None:
    """
    Return a kclique if the input graph contains a k-clique and False otherwise 
    See: https://en.wikipedia.org/wiki/Clique_problem

    """
    possible_subset = guess.guess_sublist(G, size=k)
    for i, node in enumerate(possible_subset):
        for node2 in possible_subset[i+1:]:
            if not (node, node2) in G.edges:
                return False
    return possible_subset
```

But in this case, the backtracking algorithms is much less efficient
as it will explore the full binary tree of depth k while the initial
implementation where cutting branch more agressively. 

## Graph coloration

We can implement it for graph coloration:

```python
@nondet
def kcol(guess, G: nx.Graph, colors: Iterable) -> dict | None:
    """
    return a coloration of the graph such that two vertices has distinct colors.
    """
    d = {}
    colors = set(colors)
    for node in G:
        available_color = colors.difference(d[n] for n in G[node] if n in d )
        if not available_color:
            return None
        d[node] = guess.guess_element(available_color) # this guess any colors
        
    return d
```

Remark that in this case, to be as agressive as possible we compute
all available color on the way for a given node.
