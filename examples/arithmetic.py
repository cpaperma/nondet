from nondet import nondet
from fractions import Fraction

@nondet
def composite(guess, x: int) -> tuple[int, int] | None:
    """ Return a composite of x if exists and None otherwise """
    y = guess.guess_element(range(2, x))
    if x%y == 0:
        return (y, x//y)


@nondet
def sqrt_fract_approx(guess, value: int, precision: float) -> Fraction:
    range_value = range(1, int(1/precision) + 1)
    x = guess.guess_element(range_value)
    y = guess.guess_element(range_value)
    f = Fraction(x, y)
    if abs(f**2 - value) < precision:
        return f


@nondet
def perfect_number(guess, k: int, n: int):
    perfect = guess.guess_element(range(k, n))
    divisors = list(i for i in range(1, perfect) if perfect % i == 0)
    if len(divisors) == 1:
        return 
    prod = 1
    if perfect == sum(divisors):
        return perfect

