import networkx as nx
from nondet import nondet_iter, nondet
from typing import Iterable


def _kclique(guess, G: nx.Graph, k: int) -> list | None:
    """
    Return a k-clique if the input graph contains a k-clique and False otherwise
    See: https://en.wikipedia.org/wiki/Clique_problem

    """
    possible_subset = list()
    for node in G:
        if guess():
            for node2 in possible_subset:
                if (node, node2) not in G.edges:
                    return None
            possible_subset.append(node)

        if len(possible_subset) == k:
            return possible_subset

    return None


kclique = nondet(_kclique)
kclique_iter = nondet_iter(_kclique)


@nondet
def kcol(guess, G: nx.Graph, colors: Iterable) -> dict | None:
    """
    return a coloration of the graph such that two vertices has distinct colors.
    """
    d = {}
    colors = set(colors)
    for node in G:
        available_color = colors.difference(d[n] for n in G[node] if n in d)
        if not available_color:
            return None
        d[node] = guess.guess_element(available_color)

    return d
