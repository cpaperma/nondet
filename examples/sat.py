from nondet import nondet
from functools import lru_cache


def eval_sat(formula: list[list[int]], var_assign: dict[int, bool]) -> bool:
    var_assign = dict(var_assign)
    for x, y in tuple(var_assign.items()):
        var_assign[-x] = not y

    for clause in formula:
        if all(var_assign[-e] for e in clause):
            return False
    return True

@nondet
def sat(guess, formula: list[list[int]]) -> dict[int,int] | None:
    variables = set(abs(lit) for clause in formula for lit in clause)
    d = {i: guess() for i in variables}

    if eval_sat(formula, d):
        return d


def apply_literal(formula: tuple[tuple[int]], var: int, assign: dict[int, bool]) -> tuple[tuple[int]] | None:
    nformula = []
    for clause in formula:
        if var in clause:
            continue
        if -var in clause:
            clause = tuple(filter(lambda e: e != -var, clause))
        nformula.append(clause)
    return simplify_formula(tuple(nformula), assign)

def simplify_formula(formula: tuple[tuple[int]], assign: dict[int, bool]) -> tuple[tuple[int]] | None: 
    formula = tuple(sorted(formula, key=len))
    for clause in formula:
        if not clause:
            return None
        elif len(clause) == 1:
            var = clause[0]
            assign[abs(var)] = var > 0
            return apply_literal(formula, var, assign)
    return formula
    

@nondet
def sat2(guess, formula: tuple[tuple[int]]) -> dict[int,int] | None:
    assign = {}
    while formula:
        variables = set(abs(lit) for clause in formula for lit in clause)
        var = guess.guess_element(variables)
        if guess():
            var = -var
        assign[var] = var > 0
        formula = apply_literal(formula, var, assign)

    if formula is None:
        return None
    return assign
