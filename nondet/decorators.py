from nondet.exceptions import FailedRun
from nondet.guesser import Guesser

from functools import wraps


def nondet(fct, verbose=False):
    fct_iter = nondet_iter(fct, verbose=verbose)

    @wraps(fct)
    def _fct(*args, **kwargs):
        try:
            return next(fct_iter(*args, **kwargs))
        except StopIteration:
            return None

    return _fct


def nondet_iter(fct, verbose=False):
    @wraps(fct)
    def _fct(*args, **kwargs):
        guess = Guesser()
        while True:
            if verbose:
                guess.advance()
            try:
                res = fct(guess, *args, **kwargs)
            except FailedRun:
                res = None
            if res is not None:
                yield res
            if not guess.next():
                return res

    return _fct
