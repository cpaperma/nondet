from typing import Iterable, TypeVar
from nondet.exceptions import FailedRun

T = TypeVar("T")


class Guesser:
    def __init__(self) -> None:
        self.branch: list[bool] = []
        self.position: int = 0

    def __call__(self) -> bool:
        if self.position >= len(self.branch):
            self.branch.append(False)

        self.position += 1
        return self.branch[self.position - 1]

    def advance(self):
        if True in self.branch:
            print("#" * self.branch.index(True) + " " * 20, end="\r")

    def guess_sublist(self, L: Iterable[T], size: int | None = None) -> list[T]:
        """Guess a sublist with size constraint in range `r`"""
        result = []
        for e in L:
            if self():
                result.append(e)
            if isinstance(size, int) and len(result) == size:
                return result
        raise FailedRun()

    def guess_element(self, L: Iterable[T]) -> T:
        """Guess an element in L"""
        for x in L:
            if self():
                return x
        raise FailedRun()

    def guess_pop(self, L: list[T]) -> T:
        """Guess an element in L and pop it from the list"""
        n = len(L)
        return L.pop(self.guess_number(range(n)))

    def guess_number(self, r: range) -> int:
        """Guess a number in the range"""
        return self.guess_element(r)

    def guess_permutation(self, L: Iterable[T]) -> list[T]:
        """Guess a permutation of the input list"""
        K = list(L)
        result = []
        while K:
            result.append(self.guess_pop(K))
        return result

    def next(self):
        self.branch = self.branch[: self.position+1]
        self.position = 0
        for i in range(len(self.branch)- 1, -1, -1):
            if not self.branch[i]:
                self.branch[i] = True
                return True
            self.branch[i] = False
        return False
